const assert = require('assert');
const controller = require('../../controllers/customerController'); // Update with the actual path to your controller

describe('Controller', () => {

    describe('list', () => {
        it('should call res.render with "customers"', (done) => {
            let called = false;
            const req = {
                getConnection: (callback) => callback(null, {
                    query: (sql, callback) => {
                        callback(null, [{ id: 1, name: 'Test Customer' }]);
                    }
                }),
            };
            const res = {
                render: (view, { data }) => {
                    called = true;
                    assert.strictEqual(view, 'customers');
                    assert.strictEqual(data[0].name, 'Test Customer');
                    done(); // Test ends successfully
                }
            };
            controller.list(req, res);
            assert(called); // Ensure render was called
        });
    });

    describe('save', () => {
        it('should redirect to "/" after saving', (done) => {
            const req = {
                body: { name: 'New Customer' },
                getConnection: (callback) => callback(null, {
                    query: (sql, data, callback) => {
                        assert.strictEqual(sql, 'INSERT INTO customer set ?');
                        assert.deepStrictEqual(data, req.body);
                        callback(null, { insertId: 1 });
                    }
                }),
            };
            const res = {
                redirect: (path) => {
                    assert.strictEqual(path, '/');
                    done();
                }
            };
            controller.save(req, res);
        });
    });
    describe('edit', () => {
        it('should render "customers_edit" with the correct data', (done) => {
            const req = {
                params: { id: 1 },
                getConnection: (callback) => callback(null, {
                    query: (sql, params, callback) => {
                        assert.strictEqual(sql, 'SELECT * FROM customer WHERE id = ?');
                        assert.deepStrictEqual(params, [req.params.id]);
                        callback(null, [{ id: 1, name: 'Existing Customer' }]);
                    }
                }),
            };
            const res = {
                render: (view, { data }) => {
                    assert.strictEqual(view, 'customers_edit');
                    assert.strictEqual(data.id, req.params.id);
                    done();
                }
            };
            controller.edit(req, res);
        });
    });

    describe('update', () => {
        it('should redirect to "/" after updating', (done) => {
            const req = {
                params: { id: 1 },
                body: { name: 'Updated Customer' },
                getConnection: (callback) => callback(null, {
                    query: (sql, params, callback) => {
                        assert.strictEqual(sql, 'UPDATE customer set ? where id = ?');
                        assert.deepStrictEqual(params, [req.body, req.params.id]);
                        callback(null);
                    }
                }),
            };
            const res = {
                redirect: (path) => {
                    assert.strictEqual(path, '/');
                    done();
                }
            };
            controller.update(req, res);
        });
    });
});

