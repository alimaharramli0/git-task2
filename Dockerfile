# syntax=docker/dockerfile:1
FROM node:20

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

EXPOSE 80
CMD [ "node", "./src/index.js" ]
