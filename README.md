# My Node.js Application

This is a simple guide to setting up the development environment, building the Docker image, running the application locally, and accessing the deployed application for "My Node.js Application".

## Deployed Application - http://158.101.213.207/

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Docker installed on your machine. [Get Docker](https://docs.docker.com/get-docker/)
- Docker Compose installed (comes with Docker Desktop for Mac and Windows). [Install Docker Compose](https://docs.docker.com/compose/install/)
- Node.js and npm installed if you want to run the app without Docker. [Download Node.js](https://nodejs.org/en/download/)

## Setting Up Your Development Environment

To set up your development environment:

1. Clone the repo:

    ```sh
    git clone https://gitlab.com/alimaharramli0/git-task2
    cd git-task2
    ```

2. Install the Node.js dependencies (skip if you are running everything in Docker):

    ```sh
    npm install
    ```

## Dockerfile for Node.js
```dockerfile
# syntax=docker/dockerfile:1
FROM node:20

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

EXPOSE 80
CMD [ "node", "./src/index.js" ]
```
- It starts from the official Node.js image (version 20).
- Sets the working directory to /usr/src/app.
- Copies the package.json and package-lock.json (if available) files to the container.
- Runs npm install to install the dependencies.
- Copies the rest of the app's source code to the container.
- Exposes port 80 to the Docker network.
- The default command runs the app using node ./src/index.js

## Dockerfile for MySQL DB
```dockerfile
# Use the official MySQL image as a parent image
FROM mysql:latest

# Copy the SQL script file from your host to the container's entrypoint directory
COPY setup.sql /docker-entrypoint-initdb.d/
```
- It uses the latest MySQL official image as the base.
- Copies a setup.sql file to the container, which will be executed upon container initialization to set up the database schema.

## Docker Compose

### Services
This section defines the containers that need to be created.

#### Database Service (db)
- **build: ./database:** This tells Docker to build the image for the db service using the Dockerfile located in the ./database directory.
- **environment:** Sets environment variables within the container for MySQL. This includes the database name (MYSQL_DATABASE) and the root password (MYSQL_ROOT_PASSWORD).
- **volumes:** Persists the database data by mounting db-data volume to /var/lib/mysql inside the container.
- **networks:** Attaches the container to a custom network named backend.
- **healthcheck:** Defines commands and parameters that check the health of the database service. It uses mysqladmin ping to check if the database is running and accessible.
#### Application Service (app)
- **build: .:** Docker will build the app's Docker image using the Dockerfile in the current directory.
- **depends_on:** Specifies that app service should start only after the db service is not just started, but also healthy, as indicated by the health check.
- **ports:** Maps port 80 on the host to port 80 in the container, making the application accessible via the host's IP.
- **environment:** Sets environment variables for the application to connect to the database with the necessary credentials and details (like host, user, password, port, and database name).
#### Networks
- **backend:** Defines a custom network of type bridge. Containers on the same network can communicate with each other using their service names (db and app).
#### Volumes
- **db-data:** Declares a persistent volume for the database container. This ensures that database data is persisted across container restarts and rebuilds.

## Building the Docker Image

To build the Docker image for your application, run the following command:

```sh
docker compose build
```

## Running the Application Locally

To run the application locally:

1. Start the application using Docker Compose:

```sh
docker compose up
```
2. You can access the application at http://localhost:80.

## Running Tests 

To run the tests and write results to a report:
```sh
docker compose run app npm test
```
![img_1.png](imgs/img_1.png)
### Gitlab Runner Configuration Details

![img.png](imgs/img.png)
As part of the Continuous Integration/Continuous Deployment (CI/CD) pipeline, we have configured a GitLab Runner on an Oracle Cloud instance to facilitate the deployment process. The GitLab Runner uses an SSH executor to establish a secure shell connection to the target machine where the application will be deployed.

 - For authentication, we provided a password. However, for enhanced security, it's recommended to use SSH keys instead of passwords. In production environments, passwords should be replaced with SSH key pairs and stored securely.

Once configured, the GitLab Runner will connect to the target machine using SSH for each deployment job defined in the .gitlab-ci.yml file. It executes the deployment script in the context of the trespasser user, using either the password or the private SSH key provided during setup. This method allows for automated deployment without manual intervention, streamlining the process of updating and maintaining the application in a production environment.

## .gitlab_ci.yml
```yaml
stages:
  - build
  - test
  - deploy

build:
  stage: build
  tags:
    - git_task_2
  script:
    - docker compose build
  only:
    - main

test:
  stage: test
  tags:
    - git_task_2
  script:
    - docker compose run app npm test
    - docker compose down
  only:
    - main


deploy:
  stage: deploy
  tags:
    - git_task_2
  script:
    - docker compose up -d
  only:
    - main
```
When code is committed to the main branch, the pipeline triggers automatically and runs through the stages in order:

1. **Build**: Compiles the code and builds the Docker image.
2. **Test**: Runs tests to ensure that new changes do not break the application.
3. **Deploy**: If the build and test stages pass, the pipeline deploys the application using Docker Compose.
